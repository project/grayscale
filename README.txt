﻿GrayScale: Turn your web site to Grayscale
------------------------------------------------



        ███╗   ███╗ ██████╗ ███████╗████████╗            
        ████╗ ████║██╔═══██╗██╔════╝╚══██╔══╝            
        ██╔████╔██║██║   ██║███████╗   ██║               
        ██║╚██╔╝██║██║   ██║╚════██║   ██║               
        ██║ ╚═╝ ██║╚██████╔╝███████║   ██║               
        ╚═╝     ╚═╝ ╚═════╝ ╚══════╝   ╚═╝               
                                                         
██╗   ██╗███████╗███████╗██╗     ███████╗███████╗███████╗
██║   ██║██╔════╝██╔════╝██║     ██╔════╝██╔════╝██╔════╝
██║   ██║███████╗█████╗  ██║     █████╗  ███████╗███████╗
██║   ██║╚════██║██╔══╝  ██║     ██╔══╝  ╚════██║╚════██║
╚██████╔╝███████║███████╗███████╗███████╗███████║███████║
 ╚═════╝ ╚══════╝╚══════╝╚══════╝╚══════╝╚══════╝╚══════╝
                                                         
███╗   ███╗ ██████╗ ██████╗ ██╗   ██╗██╗     ███████╗    
████╗ ████║██╔═══██╗██╔══██╗██║   ██║██║     ██╔════╝    
██╔████╔██║██║   ██║██║  ██║██║   ██║██║     █████╗      
██║╚██╔╝██║██║   ██║██║  ██║██║   ██║██║     ██╔══╝      
██║ ╚═╝ ██║╚██████╔╝██████╔╝╚██████╔╝███████╗███████╗    
╚═╝     ╚═╝ ╚═════╝ ╚═════╝  ╚═════╝ ╚══════╝╚══════╝    
                                                         



The whole purpose of this module is to be the most useless module in Drupal.

When enabled, this module adds a little CSS filter to every element 
in the site that turns the element grayscale. 

## Installaton
Install the module as you would do for other modules. 

https://www.drupal.org/docs/user_guide/en/extend-module-install.html

## Configuration
This module does not expose any configuration options. Disable the module
to bring colors back. 

..